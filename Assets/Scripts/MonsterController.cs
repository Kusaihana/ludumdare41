﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;


public class MonsterController : MonoBehaviour {

	private Vector2 _mapSize = new Vector2(12*4, 7*4);
	private Vector2 _endPosition;

	public int HealthPoint = 10;
	public List<Sprite> DifferentSkins;
	
	private PlayerController _player;
	private GameObject _cat;
	
	private void Start()
	{
		_player = FindObjectOfType<PlayerController>();
		_endPosition = transform.position;
		var ran = Random.RandomRange(0, DifferentSkins.Count);
		GetComponent<SpriteRenderer>().sprite = DifferentSkins[ran];
		_cat = GameObject.FindGameObjectWithTag("Player");
	}

	void FixedUpdate () {
		transform.position = Vector2.Lerp (transform.position, _endPosition, 1f * Time.fixedDeltaTime);

		if (HealthPoint == 0)
		{
			_player.Monsters.Remove(this);
			Destroy(gameObject);
			_player.ChangeBalance(_player.CoinBalance + 20);
			_player.ChangeScore();

		}
	}
	

	public void Gogogo(int direction, int amount)
	{
		if (direction == 0) // up
		{
			var newy = transform.position.y + amount;
			_endPosition = new Vector3(transform.position.x, Mathf.Clamp(newy, 4, _mapSize.y), 0);
		}else if(direction == 1) // down
		{
			var newy = transform.position.y - amount;  
			_endPosition = new Vector3(transform.position.x, Mathf.Clamp(newy, 4, _mapSize.y), 0);
		}else if(direction == 2) // right
		{
			var newx = transform.position.x + amount;                                                     
			_endPosition = new Vector3(Mathf.Clamp(newx, 4, _mapSize.x), transform.position.y, 0);  
		}else // left
		{
			var newx = transform.position.x - amount;                                                     
			_endPosition = new Vector3(Mathf.Clamp(newx, 4, _mapSize.x), transform.position.y, 0);  
		}
	}

	public void GogogoToPlayer(int amount)
	{
		var playerPos = _cat.transform.position;
		var dif = playerPos - transform.position;

		var newx = transform.position.x;
		var newy = transform.position.y;
		
		if (dif.x < 0 && dif.y < 0)
		{
			// go up right
			newx -= amount;
			newy -= amount;
		}else if (dif.x > 0 && dif.y < 0 )
		{
			// go down right
			newx += amount; 
			newy -= amount; 
		}else if (dif.x > 0 && dif.y > 0 )   
		{                                   
			// go down left
			newx += amount; 
			newy += amount;  
		}
		else{
			// go up left
			newx -= amount; 
			newy += amount; 
		}                                
		_endPosition = new Vector3(Mathf.Clamp(newx, 4, _mapSize.x), Mathf.Clamp(newy, 4, _mapSize.y)); 
	}

}
