﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TileController : MonoBehaviour
{
	public Sprite TurretSprite;
	
	public bool isTurretTile;

	public GameObject Turret;
	public GameObject TextMeshPrefab;

	private bool _hasTurret;
	public bool IsOccupied;
	private PlayerController _player;
	private GameObject _txtMesh;
	
	public List<GameObject> _units = new List<GameObject>();

	void OnTriggerEnter2D(Collider2D other)
	{
		var obj = other.gameObject;
		if (other.gameObject.GetComponent<TowerController>() == null)
		{
			_units.Add(other.gameObject);
			if(_player != null && _player.IsTileOccupied != null && !_player.IsTileOccupied.ContainsKey(transform.position))
				_player.IsTileOccupied.Add(transform.position, true);
			IsOccupied = true;

		}
		if (_units.Count > 1 && _txtMesh == null)
		{
			foreach (var unit in _units)
			{
				if(unit.GetComponent<MonsterController>())
					unit.transform.localScale = new Vector3(1.2f,1.2f,1f);
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D other)
	{
		var obj = other.gameObject;

		if (_units.Contains(obj))
		{
			obj.transform.localScale = new Vector3(1,1,1);
			_units.Remove(obj);
		}
			
		if (_units.Count == 0)
		{
			IsOccupied = false;
			_player.IsTileOccupied[transform.position] = false;
			Destroy(_txtMesh);
		}
	}

	private void Start()
	{
		_player = FindObjectOfType<PlayerController>();
		_player.IsTileOccupied = new Dictionary<Vector3, bool>();
	}
 
	void OnMouseDown()
	{
		if (isTurretTile && !_hasTurret)
		{
			if(_player.CoinBalance >= 50)
				BuildTurret();
			else
			{
				Debug.Log("poor thing");
			}
		}		
	}

	void BuildTurret()
	{
		GameObject obj = (GameObject)Instantiate(Turret);
		obj.transform.position = transform.position;
		_hasTurret = true;
		_player.Towers.Add(obj.GetComponent<TowerController>());
		_player.ChangeBalance(_player.CoinBalance - 50);
	}
}
