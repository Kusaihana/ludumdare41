﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

	public GameObject MonsterPrefab;
	
	private PlayerController _player;
	
	void Start()
	{
		_player = FindObjectOfType<PlayerController>();
		InvokeRepeating("Spawn", 1.0f, 5f);
	}

	void Spawn()
	{
		var posx = Random.Range(1,13);
		var posy = Random.Range(1,8);
		
		var pos = new Vector3(posx*4, posy*4, 0);
		GameObject obj = (GameObject)Instantiate(MonsterPrefab);
		obj.transform.position = pos;
		_player.Monsters.Add(obj.GetComponent<MonsterController>());
	}
}
