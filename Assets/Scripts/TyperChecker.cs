﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class TyperChecker : MonoBehaviour
{
	public Text TextToType;
	public Text TypedText;
	
	private string givenText = "asdf";

	private int charToCheck = 0;

	private StreamReader inp_stm;
	
	private PlayerController _player;

	private void Start()
	{
		_player = FindObjectOfType<PlayerController>();
	}

	private void Awake()
	{
		givenText = givenText.ToUpper();
		string file_path = "Resources/texts.txt";
		inp_stm = new StreamReader(file_path);
		StartCoroutine(GetNewText());
	}

	IEnumerator GetNewText()
	{
		TypedText.text = String.Empty;
		if(!inp_stm.EndOfStream)
		{
			string inp_ln = inp_stm.ReadLine();
			Debug.Log(inp_ln);
			givenText = inp_ln.ToUpper();			
		}
		yield return new WaitForSeconds(1);
		TextToType.text = givenText;
		charToCheck = 0;
		TextToType.color = Color.white;
	}

	void Update() {
		if (Input.anyKeyDown)
		{
			if (Input.GetMouseButtonDown(0) 
			    || Input.GetMouseButtonDown(1)
			    || Input.GetMouseButtonDown(2))
				return; //Do Nothing
			
			if(Input.GetKeyDown(KeyCode.Escape))
				Application.Quit();
			
			foreach(KeyCode vKey in System.Enum.GetValues(typeof(KeyCode))){
				if(Input.GetKey(vKey)){
					if (vKey.ToString() == givenText[charToCheck].ToString())
					{
						Debug.Log("true");
						TypedText.text += givenText[charToCheck].ToString();
						charToCheck++;
					}
					else if (Input.GetKey(KeyCode.Space))
					{
						if (givenText[charToCheck] == ' ')
						{
							Debug.Log("true");
							TypedText.text += givenText[charToCheck].ToString();
							charToCheck++;
						}
					}
					else
					{
						Debug.Log("false" + givenText[charToCheck]);
						TextToType.color = Color.red;
						_player.ChangeHealth(_player.HealthPoint - 10);
						StartCoroutine(GetNewText());
					}
                 
				}
			}

			if (charToCheck == givenText.Length)
			{
				_player.ChangeHealth(_player.HealthPoint+10);
				_player.ChangeBalance(_player.CoinBalance+10);
				TextToType.color = Color.green;
				StartCoroutine(GetNewText());
			}
		}
        
	}
}
