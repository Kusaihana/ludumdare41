﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	public int CoinBalance = 100;
	public int HealthPoint = 100;
	public int Score = 0;
	public List<TowerController> Towers;
	public List<MonsterController> Monsters;

	public Dictionary<Vector3, bool> IsTileOccupied;

	public GameObject PlayerPrefab;
	public GameObject DeathScene;
	public GameObject MainMenu;

	public GameObject EnemyController;

	public Text ScoreText;

	private int _turretCost = 100;
	private Vector2 _endPosition;
	private GameObject _cat;
	
	public delegate void ChangeEvent (int balance);
	public static event ChangeEvent changeEvent;
	
	public delegate void ChangeHealthEvent (int balance);
	public static event ChangeHealthEvent changeHealthEvent;

	private void Awake()
	{
		var posx = Random.Range(1,13);
		var posy = Random.Range(1,8);
		
		var pos = new Vector3(posx*4, posy*4, 0);
		GameObject obj = (GameObject)Instantiate(PlayerPrefab);
		_cat = obj;
		obj.transform.position = pos;
		transform.position = pos;
		_endPosition = transform.position; 
	}

	void FixedUpdate()
	{
		transform.position = Vector2.Lerp(transform.position, _endPosition, 1f * Time.fixedDeltaTime);
		_cat.transform.position = Vector2.Lerp(transform.position, _endPosition, 1f * Time.fixedDeltaTime);

		if (HealthPoint <= 0)
		{
			DeathScene.SetActive(true);
		}
	}

	public void ChangeBalance(int balance)
	{
		CoinBalance = balance;
		if(changeEvent != null)
			changeEvent(CoinBalance);
	}

	public void ChangeHealth(int health)
	{
		HealthPoint = Mathf.Clamp(health, 0, 100);
		if(changeHealthEvent != null)
			changeHealthEvent(HealthPoint);
	}

	public void Runrunrun()
	{
		var pos = GetRandomPos();
		
		while(IsTileOccupied.ContainsKey(pos) && IsTileOccupied[pos])
			pos = GetRandomPos();

		_endPosition = pos;
	}

	public Vector3 GetRandomPos()
	{
		var posx = Random.Range(1,13);
		var posy = Random.Range(1,8);
		
		return new Vector3(posx*4, posy*4);
	}

	public void ChangeScore()
	{
		Score++;
		ScoreText.text = "Score: " + Score;  
	}

	public void OnExitButtonPressed()
	{
		Application.Quit();
	}
	
	public void OnStartButtonPressed()
	{
		MainMenu.SetActive(false);
		EnemyController.SetActive(true);
	}
}
