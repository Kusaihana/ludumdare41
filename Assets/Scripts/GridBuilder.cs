﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBuilder : MonoBehaviour
{

	public Vector2 mapSize;
	public Vector2 tileSize;
	public GameObject tileObjToSpawn;
	
	
	void Start () {
		 for(int x = 0; x < mapSize.x; x++)
         {
              for(int y = 0; y < mapSize.y; y++)
              {
                   GameObject obj = (GameObject)Instantiate(tileObjToSpawn);
                   obj.transform.position = new Vector3(x*tileSize.x, y*tileSize.y);

	              if (x < 1 || x > 12 || y < 1 || y > 7)
	              {
		              obj.GetComponent<TileController>().isTurretTile = true;
		              obj.GetComponent<SpriteRenderer>().sprite = obj.GetComponent<TileController>().TurretSprite;
	              }
               }
         }
	}
}
