﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.GetComponent<MonsterController>())
		{
			_player.ChangeHealth(_player.HealthPoint - 5);
		}	
	}
	
	private PlayerController _player;

	void Start()
	{
		_player = FindObjectOfType<PlayerController>();
	}
}
