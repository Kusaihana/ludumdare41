﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RollDice : MonoBehaviour
{
	public Text RollDiceText;

	private int _moveDirection; // 0 up, 1 down, 2 right, 3 left
	private int _tileSize = 4;
	private bool _canPressed = true;

	public List<Sprite> Dices;
	public Button RollButton;

	private PlayerController _player;
	
	private void Start()
	{
		_player = FindObjectOfType<PlayerController>();
	}

	public void OnRollDiceButtonPressed()
	{
		if (_canPressed)
		{
			_canPressed = false;
			StartCoroutine(moveMonster());
			fire();
		}		
	}

	IEnumerator moveMonster()
	{
		var random = Random.Range(1, 7);
		//RollDiceText.text = "Rolled: " + random.ToString();
		RollButton.image.sprite = Dices[random-1];
		//_monster.Gogogo(_moveDirection, random * _tileSize);

		foreach (var monster in _player.Monsters)
		{
			monster.GogogoToPlayer(random * _tileSize);
		}
		
		yield return new WaitForSeconds(3);
		_player.Runrunrun();
		_canPressed = true;
	}

	void fire()
	{
		foreach (var tower in _player.Towers)
		{
			tower.Attack();	
		}
	}

	public void OnUpButtonPressed()
	{
		_moveDirection = 0;
	}
	
	public void OnDownButtonPressed()
	{
		_moveDirection = 1;
	}
	
	public void OnRightButtonPressed()
	{
		_moveDirection = 2;
	}
	
	public void OnLeftButtonPressed()
	{
		_moveDirection = 3;
	}
}
