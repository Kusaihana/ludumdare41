﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public class TowerController : MonoBehaviour
{
	public List<MonsterController> _nearbyMonsters = new List<MonsterController>();

	void OnTriggerEnter2D(Collider2D other)
	{
		var monster = other.GetComponent<MonsterController>();
		if (monster)
		{
			_nearbyMonsters.Add(monster);
			Debug.Log(_nearbyMonsters);
		}	
	}
	
	void OnTriggerExit2D(Collider2D other)
	{
		var monster = other.GetComponent<MonsterController>();
		if (monster)
		{
			_nearbyMonsters.Remove(monster);
		}	
	}

	public void Attack()
	{
		foreach (var monster in _nearbyMonsters)
		{
			monster.HealthPoint -= 5;
			StartCoroutine(ChangeColor(monster.GetComponent<SpriteRenderer>()));
		}
	}

	IEnumerator ChangeColor(SpriteRenderer mon)
	{
		mon.color = new Color(1f,0.3f,0f,0.8f);
		yield return new WaitForSeconds(0.8f);
		mon.color = Color.white;
	}
}
