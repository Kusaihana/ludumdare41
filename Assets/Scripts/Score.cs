﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

	public Text ScoreText;
	public Text HealthText;
	public Image HealthImage;

	void OnEnable(){
		PlayerController.changeEvent += BalanceChanged; 
		PlayerController.changeHealthEvent += HealthChanged; 
	}

	void BalanceChanged(int newBalance)
	{
		ScoreText.text = "Monyi: " + newBalance;
	}
	
	void HealthChanged(int newHealth)
	{
		HealthText.text = "%" + newHealth;
		HealthImage.fillAmount = newHealth / 100f;
	}
}
